# network

"""
Frederic Sales
<frederico.sales@engenharia.ufjf.br>
PPGCC - UFJF
2018
"""

import network
import ujson


class Mynetwork(object):
	"""Connect to wifi and other stuff related.
	todo:
		1. make the static ip method
		2. make ap mode method
		3. let time say what I have to do.

		json config file:
		{
			"net_ssid": "your_wifi"
			"net_pwd": "your_super_dupper_hard_to_crack_password"
			"net_mask": ""
			"net_search": "172.16.0.1"
			"net_ip": "172.16.0.22"
			"net_gateway": "172.16.0.254"
		}
	"""

	def __init__(self, self.path=None):
		"""Dummy constructor."""
		self.ssid = None
		self.passwd = None
		self.netmask  = None
		self.gateway = None
		self.search = None
		self.path = path

	def read_json_configfile(self):
		"""
		Read Json config file and do its job.
		"""
		result = []
		try:
			with open(self.path, "r") as entry:
				result = ujson.load(entry)
		except Exception as ex:
			print("I/O error ({0}): {1}\nShit, something gone wrong dude...".format(ex.errno, ex.strerror))
		else:
			entry.close()
			return result
		finally:
			pass


	def do_connect(self):
		"""And I dont know who wrote this, but it works 
		and tks you who wrote this."""
		magic = self.read_json_configfile()
		self.ssid = magic['net_ssid']
		self.passwd = magic['net_pwd']

	    sta_if = network.WLAN(network.STA_IF)
	    if not sta_if.isconnected():
	        print('connecting to network...')
	        sta_if.active(True)
	        sta_if.connect(self.ssid, self.passwd)
	        while not sta_if.isconnected():
	            pass
	    return ('network config:', sta_if.ifconfig())

	def run(self):
		"""
		Just do it...
		"""
		self.do_connect()

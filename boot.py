# network

"""
Frederic Sales
<frederico.sales@engenharia.ufjf.br>
PPGCC - UFJF
2018
"""

# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import uos, machine
#uos.dupterm(None, 1) # disable REPL on UART(0)
import gc
import webrepl
webrepl.start()
gc.collect()

# me myself and I
from my_network import *
dummy = Mynetwork("net.json")
dummy.run()
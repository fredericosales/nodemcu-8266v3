#!/bin/bash
#
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
# PPGCC - UFJF
# 2019
#
# Use
# bash write_firmware.sh /dev/ttyUSBX esp8266-2019-v1.11.bin

# ESPTOOL
ESPTOOL=`/usr/local/bin/esptool`

# erase esp previous firmware and write new one.

clear;
echo "Erasing ESP-8266"
$ESPTOOL --port=$1 erase_flash;
echo "Done";
echo 
echo "Flashing device wait..."
$ESPTOOL --port=$1 --baud 460800 write_flash --flash_size=detect -fm dio 0 $2
echo "Done."

